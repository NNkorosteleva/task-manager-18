package ru.tsc.korosteleva.tm.enumerated;

public enum Role {

    ADMIN("Adminastrator"),
    USUAL("Usual user");

    private final String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static Role toRole(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Role role : values()) {
            if (role.name().equals(value)) return role;
        }
        return null;
    }

}
