package ru.tsc.korosteleva.tm.service;

import ru.tsc.korosteleva.tm.api.repository.IAuthRepository;
import ru.tsc.korosteleva.tm.api.service.IAuthService;
import ru.tsc.korosteleva.tm.api.service.IUserService;
import ru.tsc.korosteleva.tm.exception.user.AccessDeniedException;
import ru.tsc.korosteleva.tm.exception.user.LoginEmptyException;
import ru.tsc.korosteleva.tm.exception.user.LoginPasswordIncorrectException;
import ru.tsc.korosteleva.tm.exception.user.PasswordEmptyException;
import ru.tsc.korosteleva.tm.model.User;
import ru.tsc.korosteleva.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private final IAuthRepository authRepository;

    public AuthService(IUserService userService, IAuthRepository authRepository) {
        this.userService = userService;
        this.authRepository = authRepository;
    }

    @Override
    public void login(String login, String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new LoginPasswordIncorrectException();
        authRepository.setUserId(user.getId());
    }

    @Override
    public void logout() {
        authRepository.setUserId(null);
    }

    @Override
    public User registry(String login, String password, String email) {
        return userService.create(login, password, email);
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public String getUserId() {
        final String userId = authRepository.getUserId();
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return authRepository.getUserId() != null;
    }

}
