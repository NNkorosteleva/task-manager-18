package ru.tsc.korosteleva.tm.service;

import ru.tsc.korosteleva.tm.api.repository.IUserRepository;
import ru.tsc.korosteleva.tm.api.service.IUserService;
import ru.tsc.korosteleva.tm.enumerated.Role;
import ru.tsc.korosteleva.tm.exception.user.*;
import ru.tsc.korosteleva.tm.model.User;
import ru.tsc.korosteleva.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExist(login)) throw new LoginExistException(login);
        if (isEmailExist(email)) throw new EmailExistException(email);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setEmail(email);
        user.setRole(Role.USUAL);
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email, final String role) {
        User user = create(login, password, email);
        if (role == null || role.isEmpty()) throw new RoleEmptyException();
        if (isRoleIncorrect(role)) throw new RoleIncorrectException(role);
        Role roleEnum = Role.toRole(role);
        user.setRole(roleEnum);
        return user;
    }

    @Override
    public User updateUser(final String id, final String firstName, final String lastName, final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return userRepository.updateUser(id, firstName, lastName, middleName);
    }

    @Override
    public User setPassword(String id, String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        return userRepository.setPassword(id, password);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return userRepository.findByEmail(email);
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.removeByLogin(login);
    }

    private boolean isEmailExist(final String email) {
        return findByEmail(email) != null;
    }

    private boolean isLoginExist(final String login) {
        return findByLogin(login) != null;
    }

    private boolean isRoleIncorrect(String role) {
        return Role.toRole(role) == null;
    }

}
