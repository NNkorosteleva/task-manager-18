package ru.tsc.korosteleva.tm.command.user;

import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    public static final String NAME = "user-registry";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Registry new user.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY NEW USER]");
        System.out.println("[ENTER LOGIN:]");
        String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        String password = TerminalUtil.nextLine();
        System.out.println("[ENTER EMAIL:]");
        String email = TerminalUtil.nextLine();
        getAuthService().registry(login, password, email);
    }
}
