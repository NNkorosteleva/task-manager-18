package ru.tsc.korosteleva.tm.command.task;

import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class TaskBindToProject extends AbstractTaskCommand {

    public static final String NAME = "task-bind-to-project";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Bind task to project.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().bindTaskToProject(projectId, taskId);
        System.out.println("[OK]");
    }

}
