package ru.tsc.korosteleva.tm.command.system;

import ru.tsc.korosteleva.tm.api.service.ICommandService;
import ru.tsc.korosteleva.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
