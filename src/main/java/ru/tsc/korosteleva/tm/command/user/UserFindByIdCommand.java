package ru.tsc.korosteleva.tm.command.user;

import ru.tsc.korosteleva.tm.model.User;
import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class UserFindByIdCommand extends AbstractUserCommand {

    public static final String NAME = "user-find-by-id";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Find user by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[FIND USER BY ID]");
        System.out.println("[ENTER ID:]");
        String id = TerminalUtil.nextLine();
        final User user = getUserService().findById(id);
        showUser(user);
    }
}
