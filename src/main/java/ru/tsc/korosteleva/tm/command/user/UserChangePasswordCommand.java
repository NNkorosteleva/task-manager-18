package ru.tsc.korosteleva.tm.command.user;

import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    public static final String NAME = "user-change-password";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Change user password.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE USER PASSWORD]");
        String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER NEW PASSWORD:]");
        String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }
}
