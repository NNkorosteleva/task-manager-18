package ru.tsc.korosteleva.tm.command.project;

import ru.tsc.korosteleva.tm.util.TerminalUtil;

import java.util.Date;

public class ProjectCreateCommand extends AbstractProjectCommand {

    public static final String NAME = "project-create";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Create new project.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        System.out.println("ENTER DATA BEGIN (DD.MM.YYYY):");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("ENTER DATA END (DD.MM.YYYY):");
        final Date dateEnd = TerminalUtil.nextDate();
        getProjectService().create(name, description, dateBegin, dateEnd);
    }

}
