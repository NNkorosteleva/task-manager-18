package ru.tsc.korosteleva.tm.command.task;

public class TaskClearCommand extends AbstractTaskCommand {

    public static final String NAME = "task-clear";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Remove all tasks.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");
        getTaskService().clear();
    }

}
