package ru.tsc.korosteleva.tm.command.user;

import ru.tsc.korosteleva.tm.model.User;
import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class UserFindByEmailCommand extends AbstractUserCommand {

    public static final String NAME = "user-find-by-email";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Find user by email.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[FIND USER BY EMAIL]");
        System.out.println("[ENTER EMAIL:]");
        String email = TerminalUtil.nextLine();
        final User user = getUserService().findByEmail(email);
        showUser(user);
    }
}
