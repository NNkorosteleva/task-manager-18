package ru.tsc.korosteleva.tm.api.controller;

public interface ICommandController {

    void showWelcome();

    void showAbout();

    void showVersion();

    void showHelp();

    void showSystemInfo();

    void showCommands();

    void showArguments();

    void showErrorArgument(String arg);

    void showErrorCommand(String arg);

}
