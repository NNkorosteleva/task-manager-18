package ru.tsc.korosteleva.tm.api.repository;

import ru.tsc.korosteleva.tm.model.User;

import java.util.List;

public interface IUserRepository {

    User add(User user);

    User updateUser(String id, String firstName, String lastName, String middleName);

    User setPassword(String id, String password);

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User remove(User user);

    User removeById(String id);

    User removeByLogin(String login);

}
