package ru.tsc.korosteleva.tm.api.service;

import ru.tsc.korosteleva.tm.model.User;

import java.util.List;

public interface IUserService {

    User create(String login, String password, String email);

    User create(String login, String password, String email, String role);

    User updateUser(String id, String firstName, String lastName, String middleName);

    User setPassword(String id, String password);

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeById(String id);

    User removeByLogin(String login);

}
